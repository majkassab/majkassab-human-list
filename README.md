# Majkassab-Human-List
The Majkassab Human List is sets of rules originally designed for uBlock that automatically remove/block any website which sell ads, track user, sell user data collected, any kind of cybercrime, Web Analytics, root website with empty A/AAAA record, marketing, advertisers and coin miners.

Add it to your ad blocker: https://data.majkassab.org/filterlist/majkassab-human-list.txt

Home Page: https://www.majkassab.org/filterlist.html
